<?php

namespace App\Controller;

use App\Forms\RegistroFormType;
use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }

    /**
     * @Route("/newuser", name="app_user_registration")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $em, SessionInterface $session) {

        $form = $this->createForm(RegistroFormType::class);
        $form->handleRequest($request);
        if($form->isSubmitted()&& $form->isValid()){
            $user= $form->getData();

//            if($request->request->get('registro_form')['password']!==$request->request->get('registro_form')['repeat_password']) {
//                $this->addFlash('password_error', 'Los passwords no concuerdan');
//                $session->set('email', $user->getEmail());
//                return $this->redirectToRoute('app_user_registration');
//            }

//            $regex = '/^[^0-9][_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
//            if(!preg_match($regex, $request->request->get('registro_form')['email'])) {
//                $this->addFlash('errores', 'Formato de email incorrecto');
//                return $this->redirectToRoute('app_login');
//            }

            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);


            $destination = $this->getParameter('kernel.project_dir').'/public/uploads/avatars';

            $uploadedFile = $form['avatar']->getData();
            if($uploadedFile===null) {
                $user->setAvatar('default-avatar.jpg');
            }
            else {
                $filename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = Urlizer::urlize($filename).'-'.uniqid().'.'.$uploadedFile->guessExtension();
                $uploadedFile->move($destination, $newFilename);
                $user->setAvatar($newFilename);
            }


            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Usuario guardado!');
            return $this->redirectToRoute('app_login');
         }


        return $this->render('security/registro.html.twig', [
            'registroForm' =>  $form->createView()
        ]);
    }
}
