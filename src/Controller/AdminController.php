<?php


namespace App\Controller;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{

    /**
     * @Route("/admin/users", name="admin_users")
     */
    public function allUsers(EntityManagerInterface $em)
    {
        $repo = $em->getRepository(User::class);
        $users = $repo->findAll();
        return $this->render('admin/users.html.twig', ['users' => $users]);
    }
}
