<?php


namespace App\Controller;


use App\Entity\Article;
use App\Entity\Comment;
use App\Forms\ArticuloFormType;
use App\Forms\CommentFormType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Sluggable\Util\Urlizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class ArticleController extends AbstractController
{

    /**
     * @Route("/", name="articles")
     */
    public function showAllArticles(ArticleRepository $articleRepository)
    {

        $articulos = $articleRepository->allArticlesByPublishedAtDes();

        return $this->render('articles/articles.html.twig', [
            'articulos' => $articulos
        ]);
    }

    /**
     * @Route("article/{id}", name="article")
     */
    public function showArticle($id, Request $request, EntityManagerInterface $em)
    {
        $repo = $em->getRepository(Article::class);
        $articulo = $repo->find($id);
        $comments = $articulo->getComments();

        $form = $this->createForm(CommentFormType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $repositorio = $em->getRepository(Comment::class);
            $comment = $form->getData();
            $comment->setPublishedAt(new \DateTime());
            $comment->setUser($this->getUser());
            $comment->setArticle($articulo);

            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute('article',['id' => $id]);

        }
        return $this->render('articles/article-details.html.twig', [
            'articulo' => $articulo,
            'comments' => $comments,
            'commentForm' => $form->createView()
        ]);
    }

    /**
     * @Route("articles/new", name="add_article")
     */
    public function newArticle(Request $request, EntityManagerInterface $em)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $form = $this->createForm(ArticuloFormType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $article = $form->getData();
            $article->setUser($this->getUser());
            $article->setPublishedAt(new \DateTime());


            $destination = $this->getParameter('kernel.project_dir').'/public/uploads/articles';

            $uploadedFile = $form['image']->getData();
            if($uploadedFile===null) {
                $article->setImage('article-default.png');
            }
            else {
                $filename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = Urlizer::urlize($filename).'-'.uniqid().'.'.$uploadedFile->guessExtension();
                $uploadedFile->move($destination, $newFilename);
                $article->setImage($newFilename);
            }
            $em->persist($article);
            $em->flush();
            $this->addFlash('success', "Artículo añadido correctamente");
            return $this->redirectToRoute('articles');
        }
        return $this->render('articles/article-form.html.twig', [
            "articuloForm" => $form->createView()
        ]);
    }

    /**
     * @Route("article/edit/{id}", name="article_edit")
     */
    public function editArticles($id, Request $request, EntityManagerInterface $em)
    {
        $repo = $em->getRepository(Article::class);
        $articulo = $repo->find($id);

        $form = $this->createForm(ArticuloFormType::class, $articulo);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $uploadedFile = $form['image']->getData();
            if($uploadedFile) {
                $destination = $this->getParameter('kernel.project_dir').'/public/uploads/articles';
                $filename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
                $newFilename = Urlizer::urlize($filename).'-'.uniqid().'.'.$uploadedFile->guessExtension();
                $uploadedFile->move($destination, $newFilename);
                $articulo->setImage($newFilename);
            }



            $editedArticle = $form->getData();
            $articulo->setTitle($editedArticle->getTitle());
            $articulo->setContent($editedArticle->getContent());

            $em->flush();
            $em->persist($articulo);
            return $this->redirect('/');
        }
        return $this->render('articles/article-edit.html.twig', ['articulo' => $articulo, 'form' => $form->createView()]);
    }
}
